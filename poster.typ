#let myfill = rgb("#0C2142")
#set rect(
    stroke:2pt,
	inset: 22pt,
	width: 100%
)

#set text(font: "Liberation Sans", size: 28pt)

#show figure.where(kind: image): it => align(center)[
    #block(below: 0.65em, it.body)
    #align(left)[#it.caption.body]
]

#set page(
    height: 36in,
    width: 48in,
    margin: (left: 2in, right:2in, top: 2in, bottom: 3in),
	footer-descent: 0%,
	footer: align(center)[
		#set text(white)
		#rect(height: 2in, fill:myfill)[
			#grid( columns: (1fr, 4fr, 1fr),
				image("assets/1kDlogo.png", height: 100%),
				grid( columns: (10fr, 1fr),
					[References, extended figures, and methods can be found at https://gitlab.com/kescobo/bbm-poster-2024\
					This work was funded by Wellcome LEAP 1kD],
					image("assets/qrcode.png")
				),
				image("assets/khulalogo.png", height: 100%)
			)
		]
	]

)

// Parameters

#set par(justify: true, first-line-indent: 0em)
#show par: set block(spacing: 0.65em)


#let panel(title, body, height: auto) = stack(
    rect(fill: myfill, width: 100%, text(white)[#title]),
    rect(width: 100%, height: height, body)
)  
  
#let column(..panels) = {
    let pre-last-panels = panels.pos().slice(0, -1)
    let last-panel = panels.pos().last()
    grid(
        rows: (auto,) * (panels.pos().len() - 1) + (1fr,),
        gutter: 32pt,
        ..{
          pre-last-panels.map(x => panel(x.at(0), x.at(1)))
          (panel(..last-panel, height: 100% - 1.5in),)
        }
    )
}

#grid(
    columns: (1fr,) * 3,
    rows: (auto, 1fr),
    gutter: 32pt,
    grid.cell(fill: myfill, colspan: 3)[
		#rect()[
			#set text(white)
			#set align(center)
			#grid(
				columns: (1fr, 5fr, 1fr),
				[#align(horizon)[#image("assets/vkclab-logo.png", height:5em)]],
				[
					#text(64pt)[This is your brain on bugs:] \
					#text(48pt)[Co-development of gut microbial metabolism and neural circuits in human infants] \
					Kevin S. Bonham#super()[1], Emma Margolis#super()[2], Guilherme Fahur Bottino#super()[1], The Khula Consortium, Laurel Gabard-Durnam#super()[2], Vanja Klepac-Ceraj#super()[1] \
					#super()[1]Biological Sciences, Wellesley College; Wellesely, MA.
					#super()[2]Department of Psychology,, Northeastern University; Boston, MA.
				],
				[#align(horizon)[#image("assets/pinelab-logo.png", height: 5em)]],
			)
	    	]
	    ],
  
	column(
		([== Background], [
		- Both the brain and the gut microbiome have critical windows
	      of development in the months after birth
		- The gut microbiome may influence the central nervous system
	      through the metabolism (synthesis and degradation) of neuroactive compounds
	      including neurotransmitters (eg GABA and glutamate) and short chain fatty acids
		- The development of neural circuits involves the mylenation
		  and pruning of exitory and inhibitory connections
		- Visual evoked potential (VEP) is measured by EEG,
		  and follows a predictable pattern of development
		]),
		([== Figure 1: Cohort and study design], figure(
			image("assets/figure1.png", width: 100%),
			caption: [
				*(A)* Study site in Capetown, South Africa (Gugulethu) -
				predominantly low-income community.
				*(B)* A study participant wearing EEG net.
				*(C)* Schematic of experimental design.
				Stool samples and VEP were concurrently collected
				at 3 visits in the first 16 months of life.
				Stool samples were shotgun metagenome sequenced
				to generate taxonomic and functional profiles.
				*(D)* Actual data collection for South African cohort -
				longitudinal sampling across all visits.	
			]
		)
		),
		([== Table 1: Cohort characteristics], [
			#table(
			columns: (3fr, 1fr, 1fr, 1fr),
			inset: 12pt,
			align: (left, center, center, center),
			[],													 [*Visit 1\ (N=164)*],       [*Visit 2\ (N=145)*],       [*Visit 3\ (N=124)*],
			[Mom age at birth (years; mean #sym.plus.minus SD)], [29.2 #sym.plus.minus 5.6], [28.2 #sym.plus.minus 5.6], [29.6 #sym.plus.minus 5.6], 
			[Child age (months; mean #sym.plus.minus SD)],       [3.8 #sym.plus.minus 0.9],  [8.6 #sym.plus.minus 1.5],  [14.1 #sym.plus.minus 1.0],
			[Biological sex female],         					 [69 (48.6%)], 				 [76 (52.4%)], 				 [61 (49.2%)],
			[Primary language: Xhosa], 							 [138 (97%)],				 [139, (96%)], 				 [119 (96%)],
			[Maternal ed - not high school grad], 				 [56 (39%)],				 [63 (47%)],				 [57 (46%)],
			[Maternal ed - high school grad],					 [68 (48%)],				 [64 (44%)],				 [51 (41%)],
			[Maternal income - < R1000#super([a])],				 [65 (56%)],				 [73 (50%)],				 [68 (54.8%)]
			)
			#super([a]) at the time of writing (1/16/24), 1 US Dollar = 18.87 South African Rand (ZAR).
			]
		)
	),
	column(
		([== Figure 2: VEP shows characteristic development], figure(
			image("assets/figure2.png", width: 100%),
			caption: [
			With each visit, the amplitude of each peak
			(N1 - first downward deflection, P1 - upward deflection, N2 - 2nd downward deflection)
			decreases, indicating development of the exitory / inhibitory balance,
			and the latency (time between peaks) decreases,
			indicating faster circuits (primarily as a result of myelenation).
			In subsequent analyses, P1 and N2 values for both amplitude and latencies
			are measured relative to the previous peak.
			]
		)
		),
		([== Figure 3: Microbial metabolism is associated with VEP], figure(
			image("assets/figure3.png", width: 100%),
			caption: [
				Feature Set Enrichment analyses for concurrently measured
				fecal microbiome and VEP.
				Potentially neuroactive genesets from Valles-Colomer _et. al._ (2019)
				encoded by gut-resident microbes
				were tested against all annotated genes.
				Microbial metabolism of the neurotransmitters GABA and glutamate
				were associated with the latency *(A)* and amplitude *(B)*
				of VEP peaks, especiall at later visits.
				*(C)* Microbial genes for the metabolism of short-chain fatty acids
				were also associated with VEP latency (shown) and amplitude (not shown).
				]
			)
		),

	),

	column(
		([== Figure 4: Microbial metabolism is associated with _future_ VEP], figure(
			image("assets/figure4.png", width: 100%),
			caption: [
				Feature set enrichment analyses comparing stool samples collected
				at a visit prior to VEP measurement;
				intervals between measurements are shown in *(A)*.
				Many more associations are observed for both neurotransmitters *(B-C)*
				and for SCFAs (not shown).
			]
		)
		),
		([== Conclusion], [
			- Gut microbial metabolism in early life is associated with the development
			  of early neural circuits.
			- Neurotransmitters known to be involved in early development (GABA, glutamate)
			  are particularly associated with the development of the visual circuit.
			- Short chain fatty acids, which are known to be involved in development
			  of the immune system and the brain, are also associated.
		]),
		([== Future directions], [
			- Characterize the relationship between microbial gene abundance (shown here)
			  and actual metabolite concentrations.
			  The directionality may not always be obvious - eg more degradation genes
			  for metabolite X may indicate higher X concentration (since higher conc.
			  would select for ability to degrade).
			- Experimental models showing development being affected by increases
			  or decreases in particular neuroactive metabolites.
			  VEP can be measured in rodents and follows similar developmental trajectories.
		])
	)
)

